<?php
use Dev\DemoPhpUnit\Carre;
use PHPUnit\Framework\TestCase;
class CarreTest extends TestCase
{
public function testSurface()
{
$C1 = new Carre(10);
$this->assertEquals(100, $C1->surface());
}

public function testPerimetre(){
    $C2=new Carre(4);
    $this->assertEquals(16,$C2->perimetre());

}
public function testSetCote()
{
$this->expectException(Exception::class);
$objet = new Carre(-23);
}
/**
* @dataProvider dataForTestSurface
*/
public function testSurface2($cote, $resultatAttendu)
{
$object = new Carre();
$object->setCote($cote);
$this->assertEquals($resultatAttendu, $object->surface());
}
public function dataForTestSurface()
{
return [
[0, 0],
[10, 100],
[5, 25]
];
}

}
